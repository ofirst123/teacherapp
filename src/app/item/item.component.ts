import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';
import { AngularFireList, AngularFireDatabase } from '@angular/fire/database';
import { AuthService } from '../auth.service';
import { TasksService } from '../tasks.service';
import { noop } from 'rxjs';
import { ChangeDetectorStatus } from '@angular/core/src/change_detection/constants';

@Component({
  selector: 'item',
  templateUrl: './item.component.html',
  styleUrls: ['./item.component.css']
})
export class ItemComponent implements OnInit {
  @Input() data1: any;
  @Output() myButtonClick = new EventEmitter<any>();

  name;
  key;
  studentName = this.tasksService.name;
  showAddItemButton = false;
  studentKey = this.tasksService.code;
  showEditItemFields = false;
  newItemName;
  tempItemName;
  checked: boolean;



  showText($event) {
    this.name = $event;

  }
  over() {
    this.showAddItemButton = true;
  }
  notOver() {
    this.showAddItemButton = false;
  }

  showEditItemDef() {
    this.showEditItemFields = true;
    this.tempItemName = this.name;
  }

  cancelEditItem() {
    this.showEditItemFields = false;
    this.name = this.tempItemName;

  }
  saveEditItem() {
    this.showEditItemFields = false;
    this.tasksService.saveEditItem(this.key, this.newItemName)
  }



  deleteItem() {
    this.tasksService.deleteItem(this.key);
  }

  setItemCode() {
    this.tasksService.setItemCode(this.key);
  }

  checkValue(event: any) {
    console.log(event);
    this.tasksService.changeDeploy(this.key, event, !this.checked)
  }


  constructor(public authService: AuthService, private tasksService: TasksService, private db: AngularFireDatabase) { }

  ngOnInit() {
    this.name = this.data1.name;
    this.key = this.data1.$key;
    this.studentName = this.data1.studentName;
    this.checked = this.data1.checked;
  }

}
