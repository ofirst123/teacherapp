import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../auth.service';
import { TasksService } from '../tasks.service';

@Component({
  selector: 'nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.css']
})
export class NavComponent implements OnInit {

  errM;

  constructor(private tasksService: TasksService, public authService: AuthService, private router: Router) { }


  toLogin() {
    this.tasksService.clearData()
    this.router.navigate(['/login']);
  }
  toRegister() {
    this.router.navigate(['/register']);
  }
  toLogout() {
    this.authService.logout().
      then(value => {
        this.router.navigate(['/login'])
      })
      .catch(err => {
        this.errM = err;
        console.log("blalalalalal");
      })
    this.tasksService.clearData()
  }

  toTasks() {
    this.router.navigate(['/tasks']);
  }

  ngOnInit() {
  }

}
