import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';
import { AngularFireList, AngularFireDatabase } from '@angular/fire/database';
import { AuthService } from '../auth.service';
import { TasksService } from '../tasks.service';
import { noop } from 'rxjs';
import { ChangeDetectorStatus } from '@angular/core/src/change_detection/constants';

@Component({
  selector: 'task',
  templateUrl: './task.component.html',
  styleUrls: ['./task.component.css']
})
export class TaskComponent implements OnInit {
  @Input() data: any;
  @Output() myButtonClick = new EventEmitter<any>();

  name;
  key;
  showAddDeleteTaskButtons = false;
  showEditField = false;
  showEditField1 = false;
  showAddItemFields = false;
  showAddTaskFields = false;
  showEditTaskFields = false;
  itemName;
  items = [];
  studentKey = this.tasksService.code;
  newTaskName;
  tempTaskName;
  checked: boolean = false;

  over() {
    this.showAddDeleteTaskButtons = true;
  }

  notOver() {
    this.showAddDeleteTaskButtons = false;
  }

  //item functions
  showEditItemDef() {
    this.showAddItemFields = true;
    this.tempTaskName = this.name;
    this.showAddDeleteTaskButtons = false;
  }

  saveAddItem() {
    this.showAddItemFields = false;
    this.tasksService.addItem(this.key, this.itemName, this.checked);
  }

  cancelAddItem() {
    this.showAddItemFields = false;
    this.name = this.tempTaskName;
  }

  //task functions
  showEditTaskDef() {
    this.showEditTaskFields = true
    this.tempTaskName = this.name;
  }

  cancelEditTask() {
    this.name = this.tempTaskName;
    this.showEditTaskFields = false;
  }

  saveEditTask() {
    this.showEditTaskFields = false;
    this.tasksService.saveTaskName(this.key, this.newTaskName);
  }

  deleteTask() {
    this.tasksService.delete(this.key);
    this.showEditTaskFields = false;
  }


  // deleteItem(){
  //   this.tasksService.setTaskCode(this.key)
  //  this.tasksService.deleteItem(this.studentKey, this.key);
  // }
  setTaskCode() {
    this.tasksService.setTaskCode(this.key)
    console.log(this.tasksService.code)
    console.log(this.tasksService.name)
  }

  constructor(public authService: AuthService, private tasksService: TasksService, private db: AngularFireDatabase) { }

  ngOnInit() {
    this.name = this.data.name;
    this.key = this.data.$key;

    if (this.studentKey != null) {
      this.authService.user.subscribe(user => {
        this.db.list('/users/' + this.studentKey + '/tasks/' + this.key + '/items/').snapshotChanges().subscribe(
          items => {
            this.items = [];
            items.forEach(
              item => {
                let y = item.payload.toJSON();
                y["$key"] = item.key;
                this.items.push(y);
              }
            )
          }
        )
      })
    }
    else {
      this.authService.user.subscribe(user => {
        this.db.list('/users/' + user.uid + '/tasks/' + this.key + '/items/').snapshotChanges().subscribe(
          items => {
            this.items = [];
            items.forEach(
              item => {
                let y = item.payload.toJSON();
                y["$key"] = item.key;
                this.items.push(y);
              }
            )
          }
        )
      })
    }
  }
}
