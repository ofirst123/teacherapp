import { Injectable } from '@angular/core';
import { AuthService } from './auth.service';
import { AngularFireList, AngularFireDatabase } from '@angular/fire/database';

@Injectable({
  providedIn: 'root'
})
export class TasksService {

  public code;
  public name;
  public itemCode;
  public taskCode;

  addTask(newTask) {
    this.authService.user.subscribe(user => {
      this.db.list('/users/' + user.uid + '/tasks/').push({ 'name': newTask });
    })
  }

  delete(key) {
    this.authService.user.subscribe(user => {
      this.db.list('/users/' + user.uid + '/tasks/').remove(key);
    })
  }

  saveEditItem(key, newItemName) {
    this.authService.user.subscribe(user => {
      this.db.list('/users/' + user.uid + '/tasks/' + this.taskCode + '/items/').update(key, { 'name': newItemName });
    })
  }

  deleteItem(key) {
    this.authService.user.subscribe(user => {
      this.db.list('/users/' + user.uid + '/tasks/' + this.taskCode + '/items/').remove(key);
    })
  }

  addItem(key, itemName, checked) {
    this.authService.user.subscribe(user => {
      this.db.list('/users/' + user.uid + '/tasks/' + key + '/items/').push({ 'name': itemName, 'studentName': '', 'checked': checked });
    })
  }

  showTasks(code: string) {
  }

  setData(code, name) {
    this.code = code;
    this.name = name;
  }
  setData2(name) {
    this.name = name;
    this.authService.user.subscribe(user => {
      this.code = user.uid;
    })
  }

  setTaskCode(taskCode) {
    this.taskCode = taskCode;
  }

  setItemCode(itemCode) {
    this.itemCode = itemCode;
  }

  getData() {
    let temp = this.code;
    this.clearData();
    return temp;
  }

  clearData() {
    this.code = undefined;
    this.name = undefined;
  }

  saveTaskName(key, name) {
    this.authService.user.subscribe(user => {
      this.db.list('/users/' + user.uid + '/tasks/').update(key, { 'name': name });
    })
  }

  changeDeploy(key, event, checked) {
    console.log("working")
    if (checked) {
      this.authService.user.subscribe(user => {
        this.db.list('/users/' + this.code + '/tasks/' + this.taskCode + '/items/').update(key, { 'studentName': this.name, 'checked': checked });
      })
    }
    else if (!checked) {
      this.authService.user.subscribe(user => {
        this.db.list('/users/' + this.code + '/tasks/' + this.taskCode + '/items/').update(key, { 'studentName': '', 'checked': checked });
      })
    }
  }

  //  saveItemName(key,name){
  //    this.authService.user.subscribe(user=>{
  //     this.db.list('/users/'+user.uid+'/tasks/'+this.taskCode+'/items/').update(key,{'name':name});
  //  })
  // }

  constructor(private authService: AuthService, private db: AngularFireDatabase) { }
}