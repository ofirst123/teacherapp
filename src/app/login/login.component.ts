import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { TasksService } from '../tasks.service';
import { AuthService } from '../auth.service';
import { Router } from '@angular/router';
import { FormControl, FormGroupDirective, NgForm, Validators } from '@angular/forms';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {


  email = new FormControl('', [Validators.required, Validators.email]);
  email1: string;
  password: string;
  errM;
  code: string;
  whoami: string = "";
  name: string;
  types: string[] = ['Teacher', 'Student'];


  getErrorMessage() {
    return this.email.hasError('required') ? 'You must enter a value' :
      this.email.hasError('email') ? 'Not a valid email' :
        '';
  }

  constructor(public authService: AuthService, private tasksService: TasksService, private router: Router) { }

  ngOnInit() {
  }

  login() {
    if (this.code != null && this.name != null) {
      this.tasksService.setData(this.code, '- Chosen by student: ' + this.name);
      this.router.navigate(['/tasks']);
    }
    else {
      if ((this.email1 == null || this.password == null) && (this.code == null))
        this.errM = "You must fill all the fields"
      return this.authService.login(this.email1, this.password)
        .then(value => { this.tasksService.setData2('- Chosen by the Teacher'); })
        .then(value => {

          this.router.navigate(['/tasks']);



        })
        .catch(err => {
          this.errM = err;
          console.log("blalalalalal");
        })
    }
  }
}
