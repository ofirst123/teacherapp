import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { AngularFireList, AngularFireDatabase } from '@angular/fire/database';
import { AuthService } from '../auth.service';
import { TasksService } from '../tasks.service';

@Component({
  selector: 'tasks',
  templateUrl: './tasks.component.html',
  styleUrls: ['./tasks.component.css']
})
export class TasksComponent implements OnInit {
  // @Input() data2:any;
  @Output() myButtonClick = new EventEmitter<any>();

  tasks = [];
  name;
  classKey;
  studentKey = this.tasksService.code;
  studentName = this.tasksService.name;
  taskName;

  showText($event) {
    this.name = $event;
  }

  constructor(public authService: AuthService, private tasksService: TasksService, private db: AngularFireDatabase) { }

  ngOnInit() {
    if (this.studentKey) {
      this.classKey = this.studentKey;
      this.db.list('/users/' + this.studentKey + '/tasks/').snapshotChanges().subscribe(
        tasks => {
          this.tasks = [];
          tasks.forEach(
            item => {
              let y = item.payload.toJSON();
              y["$key"] = item.key;
              this.tasks.push(y);
            }
          )
        }
      )
    }
    else {
      this.authService.user.subscribe(user => {
        this.classKey = user.uid;
        this.db.list('/users/' + user.uid + '/tasks/').snapshotChanges().subscribe(
          tasks => {
            this.tasks = [];
            tasks.forEach(
              item => {
                let y = item.payload.toJSON();
                y["$key"] = item.key;
                this.tasks.push(y);
              }
            )
          }
        )
      })
    }
  }

  addTask() {
    this.tasksService.addTask(this.taskName);
    console.log(this.taskName);
    this.taskName = ' ';
  }
}