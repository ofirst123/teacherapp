import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {Routes, RouterModule} from '@angular/router';

import { AppComponent } from './app.component';
import { MainComponent } from './main/main.component';
import { LoginComponent } from './login/login.component';
import { NavComponent } from './nav/nav.component';
import { RegisterComponent } from './register/register.component';
import { TasksComponent } from './tasks/tasks.component';
import { TaskComponent } from './task/task.component';

import {MatButtonToggleModule} from '@angular/material/button-toggle';
import {MatCardModule} from '@angular/material/card';
import {MatInputModule} from '@angular/material/input';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MatFormFieldModule} from '@angular/material/form-field';
import {ReactiveFormsModule} from '@angular/forms';
import {MatButtonModule} from '@angular/material/button';
import {MatListModule} from '@angular/material/list';
import { FormsModule} from '@angular/forms';
import {MatRadioModule} from '@angular/material/radio';
import {MatCheckboxModule} from '@angular/material/checkbox';

import { AngularFireModule } from '@angular/fire';
import { AngularFireDatabaseModule } from '@angular/fire/database';
import { AngularFireAuthModule } from '@angular/fire/auth';

import { environment} from '../environments/environment';
import { WelcomeComponent } from './welcome/welcome.component';
import { ItemComponent } from './item/item.component';


@NgModule({
  declarations: [
    AppComponent,
    MainComponent,
    LoginComponent,
    NavComponent,
    RegisterComponent,
    TasksComponent,
    TaskComponent,
    WelcomeComponent,
    ItemComponent
  ],
  imports: [
    BrowserModule,
    MatCardModule,
    FormsModule,
   MatInputModule,
   MatFormFieldModule,
   BrowserAnimationsModule,
   ReactiveFormsModule,
   MatButtonModule,
   MatListModule, 
   MatRadioModule,
   MatCheckboxModule,
   AngularFireModule.initializeApp(environment.firebase),
   AngularFireDatabaseModule,
   AngularFireAuthModule,
    RouterModule.forRoot([
      {path:'', component:LoginComponent},
      {path:'register', component:RegisterComponent},
      {path:'login', component:LoginComponent},
      {path:'tasks', component:TasksComponent},
      {path:'**', component:LoginComponent} 
    ])
 
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
